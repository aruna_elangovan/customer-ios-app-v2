////
////  TutorialView.swift
////  21North
////
////  Created by Aruna Elangovan on 17/07/18.
////  Copyright © 2018 21North. All rights reserved.
////
//
//import UIKit
//
//class TutorialView: UIView{
//    var tutorialHeaderLabel:UILabel!
//    var tutorialImageView:UIImageView!
//    var tutorialTextView:UITextView!
//    var tutorialStartButton:UIButton!
//    var tutorialPageControl:UIPageControl!
//    var style = Style()
//    
//    var scrollView  = UIScrollView.newAutoLayout()
//    var contentView = UIView.newAutoLayout()
//    
//    override init(frame: CGRect) {
//        super.init(frame: frame)
//        setupUI()
//    }
//    
//    required init?(coder aDecoder: NSCoder) {
//        super.init(coder: aDecoder)
//        setupUI()
//    }
//    
//    //MARK: UI setup
//    func setupUI(){
//        self.backgroundColor = UIColor.white
//        
//        self.addSubview(scrollView)
//        scrollView.translatesAutoresizingMaskIntoConstraints = false
//        scrollView.autoPinEdgesToSuperviewEdges(with: (UIEdgeInsetsMake(30, 0.0, 0.0, 0.0)))
//        
//        scrollView.addSubview(contentView)
//        contentView.translatesAutoresizingMaskIntoConstraints = false
//        contentView.autoPinEdgesToSuperviewEdges()
//        contentView.autoMatch(.width, to: .width, of: self)
//        contentView.isUserInteractionEnabled = true
//        
//        tutorialHeaderLabel = UILabel()
//        contentView.addSubview(tutorialHeaderLabel)
//        tutorialHeaderLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: 30.0)
//        tutorialHeaderLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 30.0)
//        tutorialHeaderLabel.autoPinEdge(toSuperviewEdge: .top, withInset: 30)
//        tutorialHeaderLabel.autoSetDimension(.height, toSize: 20.0, relation: .greaterThanOrEqual)
//        //        tutorialHeaderLabel.text = NSLocalizedString("Edit", comment: "")
//        tutorialHeaderLabel.translatesAutoresizingMaskIntoConstraints = false
//        tutorialHeaderLabel.font = style.editCarTextFieldFont
//        tutorialHeaderLabel.textColor = style.labelFontColor
//        //        headerLabel.isUserInteractionEnabled = true
//        tutorialHeaderLabel.textAlignment = .center
//        
//        tutorialImageView = UIImageView()
//        contentView.addSubview(tutorialImageView)
//        tutorialImageView.autoSetDimension(.width, toSize: 82.0)
//        tutorialImageView.autoSetDimension(.height, toSize: 82.0)
//        tutorialImageView.autoPinEdge(.top, to: .bottom, of: tutorialHeaderLabel, withOffset: 20.0)
//        tutorialImageView.autoAlignAxis(.vertical, toSameAxisOf: self, withOffset: 0.0)
//        tutorialImageView.layer.cornerRadius = 41
//        tutorialImageView.clipsToBounds = true
//        tutorialImageView.backgroundColor = UIColor.gray
//        tutorialImageView.translatesAutoresizingMaskIntoConstraints = false
//        
//        
//        tutorialTextView = UITextView()
//        contentView.addSubview(tutorialTextView)
//        tutorialTextView.autoPinEdge(toSuperviewEdge: .leading, withInset: 20.0)
//        tutorialTextView.autoPinEdge(toSuperviewEdge: .trailing, withInset: 20.0)
//        tutorialTextView.autoPinEdge(.top, to: .bottom, of: tutorialImageView, withOffset: 20.0)
//        tutorialTextView.autoSetDimension(.height, toSize: 20.0, relation: .greaterThanOrEqual)
//        tutorialTextView.textAlignment = .center
//        let textFieldTextColor = style.labelFontColor
//        tutorialTextView.textColor = textFieldTextColor
//        tutorialTextView.font = style.editCarTextFieldFont
//        
//        
//        tutorialStartButton = UIButton()
//        contentView.addSubview(tutorialStartButton)
//        tutorialStartButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: -50.0)
//        //        tutorialStartButton.autoPinEdge(.top, to: .bottom, of: tutorialImageView, withOffset: 100.0)
//        tutorialStartButton.autoSetDimension(.width, toSize: 100.0, relation: .greaterThanOrEqual)
//        tutorialStartButton.autoSetDimension(.height, toSize: 20.0, relation: .greaterThanOrEqual)
//        tutorialStartButton.autoAlignAxis(.vertical, toSameAxisOf: self, withOffset: 0.0)
//        tutorialStartButton.layer.cornerRadius = 10
//        tutorialStartButton.backgroundColor = UIColor.red
//        tutorialStartButton.titleLabel?.textColor = UIColor.white
//        tutorialStartButton.setTitle("Get started", for: .normal)
//        //        emailTextField.font = style.editCarTextFieldFont
//        
//        tutorialPageControl = UIPageControl()
//        contentView.addSubview(tutorialPageControl)
//        tutorialPageControl.autoPinEdge(.bottom, to: .top, of: tutorialStartButton, withOffset: 20.0)
//        tutorialPageControl.autoSetDimension(.height, toSize: 20.0, relation: .greaterThanOrEqual)
//    }
//}
