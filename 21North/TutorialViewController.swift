//
//  TutorialViewController.swift
//  21North
//
//  Created by Aruna Elangovan on 17/07/18.
//  Copyright © 2018 21North. All rights reserved.
//

import UIKit

class TutorialViewController: BaseViewController, UIScrollViewDelegate {

    var tutorialHeaderLabel:UILabel!
    var tutorialImageView:UIImageView!
    var tutorialTextView:UILabel!
    var tutorialStartButton:UIButton!
    var tutorialPageControl:UIPageControl!
    
    var currentIndex = 0
    
    var scrollView  = UIScrollView.newAutoLayout()
    let appdelegate = UIApplication.shared.delegate as! AppDelegate
    let style = Style()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        setupUI()
    }
    
    //MARK: UI setup
    func setupUI(){
        DataCreator.sharedInstance.currentScreen = "TutorialScreen"
        view.backgroundColor = UIColor.white
        
        view.addSubview(scrollView)
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.autoPinEdgesToSuperviewEdges(with: (UIEdgeInsetsMake(navigationHeight, 0.0, 0.0, 0.0)))
        
        tutorialHeaderLabel = UILabel()
        view.addSubview(tutorialHeaderLabel)
        tutorialHeaderLabel.autoPinEdge(toSuperviewEdge: .leading, withInset: 30.0)
        tutorialHeaderLabel.autoPinEdge(toSuperviewEdge: .trailing, withInset: 30.0)
        tutorialHeaderLabel.autoPinEdge(toSuperviewEdge: .top, withInset: navigationHeight)
        tutorialHeaderLabel.autoSetDimension(.height, toSize: 20.0, relation: .greaterThanOrEqual)
        tutorialHeaderLabel.translatesAutoresizingMaskIntoConstraints = false
        tutorialHeaderLabel.font = style.editCarTextFieldFont
        tutorialHeaderLabel.textColor = style.labelFontColor
        tutorialHeaderLabel.textAlignment = .center
        
        tutorialImageView = UIImageView()
        view.addSubview(tutorialImageView)
        tutorialImageView.autoSetDimension(.width, toSize: 82.0)
        tutorialImageView.autoSetDimension(.height, toSize: 82.0)
        tutorialImageView.autoPinEdge(.top, to: .bottom, of: tutorialHeaderLabel, withOffset: 20.0)
        tutorialImageView.autoAlignAxis(.vertical, toSameAxisOf: self.view, withOffset: 0.0)
        tutorialImageView.layer.cornerRadius = 41
        tutorialImageView.clipsToBounds = true
        tutorialImageView.backgroundColor = UIColor.gray
        tutorialImageView.translatesAutoresizingMaskIntoConstraints = false
        
        tutorialTextView = UILabel()
        view.addSubview(tutorialTextView)
        tutorialTextView.autoPinEdge(toSuperviewEdge: .leading, withInset: 20.0)
        tutorialTextView.autoPinEdge(toSuperviewEdge: .trailing, withInset: 20.0)
        tutorialTextView.autoPinEdge(.top, to: .bottom, of: tutorialImageView, withOffset: 20.0)
        tutorialTextView.autoSetDimension(.height, toSize: 100.0, relation: .greaterThanOrEqual)
        tutorialTextView.textAlignment = .center
        let textFieldTextColor = style.labelFontColor
        tutorialTextView.textColor = textFieldTextColor
        tutorialTextView.font = style.editCarTextFieldFont
        tutorialTextView.numberOfLines = 0
        tutorialTextView.lineBreakMode = .byWordWrapping
        
        tutorialPageControl = UIPageControl()
        view.addSubview(tutorialPageControl)
        tutorialPageControl.autoPinEdge(.top, to: .bottom, of: tutorialTextView, withOffset: 20.0)
        tutorialPageControl.autoSetDimension(.width, toSize: 200.0, relation: .greaterThanOrEqual)
        tutorialPageControl.autoSetDimension(.height, toSize: 25.0, relation: .greaterThanOrEqual)
        tutorialPageControl.autoAlignAxis(.vertical, toSameAxisOf: self.view, withOffset: 0.0)
        
        
        tutorialStartButton = UIButton()
        view.addSubview(tutorialStartButton)
        tutorialStartButton.autoPinEdge(toSuperviewEdge: .bottom, withInset: 40.0)
        tutorialStartButton.autoPinEdge(toSuperviewEdge: .leading, withInset: 30.0)
        tutorialStartButton.autoPinEdge(toSuperviewEdge: .trailing, withInset: 30.0)
        tutorialStartButton.autoSetDimension(.height, toSize: 30.0, relation: .greaterThanOrEqual)
        tutorialStartButton.layer.cornerRadius = 15
        tutorialStartButton.backgroundColor = UIColor.red
        tutorialStartButton.titleLabel?.textColor = UIColor.white
        tutorialStartButton.setTitle("Get started", for: .normal)
        tutorialStartButton.autoAlignAxis(.vertical, toSameAxisOf: self.view, withOffset: 0.0)
        
        scrollView.frame = CGRect(x:0, y:0, width:self.view.frame.width, height:self.view.frame.height)
        let scrollViewWidth:CGFloat = self.scrollView.frame.width
        let scrollViewHeight:CGFloat = self.scrollView.frame.height

        let pageCount : CGFloat = 4
        scrollView.contentSize = CGSize(width:self.scrollView.frame.width * pageCount, height:self.scrollView.frame.height)
        scrollView.delegate = self
        tutorialPageControl.currentPage = 0
        tutorialPageControl.pageIndicatorTintColor = UIColor.blue
        tutorialPageControl.currentPageIndicatorTintColor = UIColor.red
        
        let image = UIImage.outlinedEllipse(size: CGSize(width: 7.0, height: 7.0), color: .blue)
        tutorialPageControl.pageIndicatorTintColor = UIColor.init(patternImage: image!)
        tutorialPageControl.currentPageIndicatorTintColor = .blue
        tutorialPageControl.transform = CGAffineTransform(scaleX: 2, y: 2);
        
        tutorialImageView.image = UIImage(named: "Tutorial-Servicing")
        tutorialHeaderLabel.text = "CAR SERVICING"
        tutorialTextView.text = "Servicing tutorial page ......sdfcsfcsd adada da da da da sdas dasdsvdfvascad d cfas cas c asc as cas c ascas cas c asc asc asc asc a cas csacasc sacas cascas cascsacascascascascascasc  a sca sc asc as ca sc asc"
        
        scrollView.isPagingEnabled = true
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.showsVerticalScrollIndicator = false
        
        tutorialPageControl.numberOfPages = Int(pageCount)
        tutorialPageControl.isUserInteractionEnabled = true
        tutorialPageControl.addTarget(self, action: #selector(self.pageChanged), for: .valueChanged)
        
//        for i in 0..<Int(pageCount) {
//            print(self.scrollView.frame.size.width)
//            let image = UIImageView(frame: CGRectMake(self.scrollView.frame.size.width * CGFloat(i), 0, self.scrMain.frame.size.width, self.scrMain.frame.size.height))
//            image.image = UIImage(named: "Tutorial-Servicing")
//            image.contentMode = UIViewContentMode.ScaleAspectFit
//            self.scrMain.addSubview(image)
    }
    
    //MARK: Page tap action
    @objc func pageChanged() {
        // Move to Right
        //You can use your UICollectionView variable too instead of my scrollview variable
        self.scrollView
            .scrollRectToVisible(CGRect(
                x: Int(self.scrollView.frame.size.width) * tutorialPageControl.currentPage,
                y: 0,
                width:Int(self.scrollView.frame.size.width),
                height: Int(self.scrollView.frame.size.height)),
                                 animated: true)
        
        changePageContent(currentPage: tutorialPageControl.currentPage)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func changePageContent(currentPage: Int){
        // Change the text accordingly
        if Int(currentPage) == 0{
            self.tutorialImageView.image = UIImage(named: "Tutorial-Servicing")
            self.tutorialHeaderLabel.text = "CAR SERVICING"
            self.tutorialTextView.text = "Servicing tutorial page ......sdfcsfcsd adada da da da da sdas dasdsvdfvascad d cfas cas c asc as cas c ascas cas c asc asc asc asc a cas csacasc sacas cascas cascsacascascascascascasc  a sca sc asc as ca sc asc"
        }else if Int(currentPage) == 1{
            self.tutorialHeaderLabel.text = "ROAD SIDE ASSISTANCE"
            self.tutorialTextView.text = "RSA tutorial page......sdfcsfcsd adada da da da da sdas dasdsvdfvascad d cfas cas c asc as cas c ascas cas c asc asc asc asc a cas csacasc sacas cascas cascsacascascascascascasc  a sca sc asc as ca sc asc"
        }else if Int(currentPage) == 2{
            self.tutorialHeaderLabel.text = "CHAUFFEUR SERVICE"
            self.tutorialTextView.text = "Chauffeur tutorial page......sdfcsfcsd adada da da da da sdas dasdsvdfvascad d cfas cas c asc as cas c ascas cas c asc asc asc asc a cas csacasc sacas cascas cascsacascascascascascasc  a sca sc asc as ca sc asc"
        }else{
            self.tutorialHeaderLabel.text = "OTHER SERVICES"
            self.tutorialTextView.text = "Other tutorial page......sdfcsfcsd adada da da da da sdas dasdsvdfvascad d cfas cas c asc as cas c ascas cas c asc asc asc asc a cas csacasc sacas cascas cascsacascascascascascasc  a sca sc asc as ca sc asc"
            // Show the "Let's Start" button in the last slide (with a fade in animation)
            UIView.animate(withDuration: 1.0, animations: { () -> Void in
                self.tutorialStartButton.alpha = 1.0
            })
        }
    }
}
private typealias ScrollView = TutorialViewController
extension ScrollView
{
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView){
        // Test the offset and calculate the current page after scrolling ends
        let pageWidth:CGFloat = scrollView.frame.width
        let currentPage:CGFloat = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1
        // Change the indicator
        self.tutorialPageControl.currentPage = Int(currentPage);
        //Change page content
        self.changePageContent(currentPage: Int(currentPage))
    }
}

extension UIImage {
    class func outlinedEllipse(size: CGSize, color: UIColor, lineWidth: CGFloat = 1.0) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        guard let context = UIGraphicsGetCurrentContext() else {
            return nil
        }
        
        context.setStrokeColor(color.cgColor)
        context.setLineWidth(lineWidth)
        let rect = CGRect(origin: .zero, size: size).insetBy(dx: lineWidth * 0.5, dy: lineWidth * 0.5)
        context.addEllipse(in: rect)
        context.strokePath()
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
}

//extension UIPageControl {
//
//    func customPageControl(dotFillColor:UIColor, dotBorderColor:UIColor, dotBorderWidth:CGFloat) {
//        for (pageIndex, dotView) in self.subviews.enumerated() {
//            if self.currentPage == pageIndex {
//                dotView.backgroundColor = dotFillColor
//                dotView.layer.cornerRadius = dotView.frame.size.height / 2
//            }else{
//                dotView.backgroundColor = .clear
//                dotView.layer.cornerRadius = dotView.frame.size.height / 2
//                dotView.layer.borderColor = dotBorderColor.cgColor
//                dotView.layer.borderWidth = dotBorderWidth
//            }
//        }
//    }
//
//}

